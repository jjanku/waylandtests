#include <stdio.h>

// GTK includes
#include <glib.h>
#include <gtk/gtk.h>
#include <gio/gio.h>

// DRM includes
#include <xf86drm.h>
#include <xf86drmMode.h>

// X11 includes
#include <X11/extensions/Xrandr.h>

#define MAX_SCREENS 16



// DRM modules to try
const char *modules[] = {
        "qxl",
        "i915",
        NULL
};


// MUTTER DBUS FORMAT STRINGS
#define MODE_BASE_FORMAT "siiddad"
#define MODE_FORMAT "(" MODE_BASE_FORMAT "a{sv})"
#define MODES_FORMAT "a" MODE_FORMAT
#define MONITOR_SPEC_FORMAT "(ssss)"
#define MONITOR_FORMAT "(" MONITOR_SPEC_FORMAT MODES_FORMAT "a{sv})"
#define MONITORS_FORMAT "a" MONITOR_FORMAT

#define LOGICAL_MONITOR_MONITORS_FORMAT "a" MONITOR_SPEC_FORMAT
#define LOGICAL_MONITOR_FORMAT "(iidub" LOGICAL_MONITOR_MONITORS_FORMAT "a{sv})"
#define LOGICAL_MONITORS_FORMAT "a" LOGICAL_MONITOR_FORMAT

#define CURRENT_STATE_FORMAT "(u" MONITORS_FORMAT LOGICAL_MONITORS_FORMAT "a{sv})"


void standard_print(const char *name, unsigned int width, unsigned int height, int x, int y)
{
    printf("%s - %ux%u+%u+%u\n", name, width, height, x, y);
}

void mutter_log_display_info(gboolean verbose)
{
    GError *error = NULL;
    GDBusConnection *dbus_conn = g_bus_get_sync(G_BUS_TYPE_SESSION, NULL, &error);

    if (!dbus_conn) {
        printf("display: failed to connect to dbus: %s\n", error->message);
        g_clear_error(&error);
        return ;
    }

    GDBusProxy *dbus_proxy = g_dbus_proxy_new_sync(dbus_conn,
                                       G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START,
                                       NULL,
                                       "org.gnome.Mutter.DisplayConfig",
                                       "/org/gnome/Mutter/DisplayConfig",
                                       "org.gnome.Mutter.DisplayConfig",
                                       NULL,
                                       &error);
    if (!dbus_proxy) {
        printf("display: failed to create dbus proxy: %s\n", error->message);
        g_clear_error(&error);
        g_dbus_connection_close_sync(dbus_conn, NULL, NULL);
        g_object_unref(dbus_conn);
        return;
    }

    GVariant *values = g_dbus_proxy_call_sync(dbus_proxy,
                                              "GetCurrentState",
                                              NULL,
                                              G_DBUS_CALL_FLAGS_NONE,
                                              -1,   // use proxy default timeout
                                              NULL,
                                              &error);
    if (!values) {
        printf("display: failed to call GetCurrentState from mutter over DBUS\n");
        if (error != NULL) {
            printf("=> error message: %s\n", error->message);
            g_clear_error(&error);
        }
    }
    else {
        guint32 serial;
        GVariantIter *monitors = NULL;
        GVariantIter *logical_monitors = NULL;
        GVariantIter *properties = NULL;

        g_variant_get(values, CURRENT_STATE_FORMAT, &serial, &monitors, &logical_monitors, &properties);

        if (!monitors)
            printf("monitor is NULL\n");
        if (!logical_monitors)
            printf("logical_monitor is NULL\n");
        if (!properties)
            printf("properties is NULL\n");

        // list monitors
        GVariant *monitor = NULL;
        while (g_variant_iter_next(monitors, "@"MONITOR_FORMAT, &monitor)) {

            gchar *connector, *vendor, *name, *serial;
            GVariantIter *modes = NULL;
            GVariantIter *props = NULL;

            g_variant_get(monitor, MONITOR_FORMAT, &connector, &vendor, &name, &serial, &modes, &props);

            // list modes
            GVariant *mode = NULL;
            while (g_variant_iter_next(modes, "@"MODE_FORMAT, &mode)) {
                GVariant *scales = NULL;
                GVariant *properties = NULL;
                char *id;
                int width;
                int height;
                double refresh_rate;
                double preferred_scale;

                g_variant_get(mode, "(" MODE_BASE_FORMAT "@a{sv})",
                              &id, &width, &height, &refresh_rate, &preferred_scale, &scales, &properties);

                gboolean is_current;

                if (!g_variant_lookup(properties, "is-current", "b", &is_current))
                    is_current = FALSE;

                if (!is_current)
                    continue;

                // find the logical monitor using this connector, in order to get the propper x and y coordinates
                int x, y;
                GVariant *logical_monitor = NULL;
                GVariantIter *logical_monitor_iterator = g_variant_iter_copy(logical_monitors);
                while (g_variant_iter_next(logical_monitor_iterator, "@"LOGICAL_MONITOR_FORMAT, &logical_monitor)) {
                    GVariant *scale = NULL;
                    GVariant *transform = NULL;
                    GVariant *primary = NULL;
                    GVariantIter *tmp_monitors = NULL;
                    GVariant *properties = NULL;

                    g_variant_get(logical_monitor, LOGICAL_MONITOR_FORMAT, &x, &y, &scale, &transform, &primary, &tmp_monitors, &properties);

                    GVariant *tmp_monitor = NULL;
                    gboolean found = FALSE;
                    while (g_variant_iter_next(tmp_monitors, "@"MONITOR_SPEC_FORMAT, &tmp_monitor)) {
                        gchar *tmp_connector, *tmp_vendor, *tmp_name, *tmp_serial;

                        g_variant_get(tmp_monitor, MONITOR_SPEC_FORMAT, &tmp_connector, &tmp_vendor, &tmp_name, &tmp_serial);

                        if (strcmp(connector, tmp_connector) == 0) {
                            //printf("Logical monitor found at %d,%d\n", x, y);
                            found = TRUE;
                            break ;
                        }
                    }

                    if (found)
                        break;
                    x = y = 0;
                }

                standard_print(connector, width, height, x, y);
            }

        }

        g_variant_unref(values);
    }

    g_dbus_connection_close_sync(dbus_conn, NULL, NULL);
    g_object_unref(dbus_proxy);
}

void gtk_log_display_info(gboolean verbose, gboolean open_new_display)
{
    GdkDisplay *display;
    gint num_monitors;
    static int nb = 0;

    if (open_new_display) {
        const gchar *display_name = gdk_display_get_name(gdk_display_get_default());
        display = gdk_display_open(display_name);
        if (!display) {
            printf("Could not reopen display %s after %d loops\n", display_name, nb);
            return;
        }

        nb++;
        if (verbose)
            printf("Loop %d\n", nb);
    }
    else {
        display = gdk_display_get_default();
    }
    gdk_display_sync(display);

#ifdef GTK3
    num_monitors = gdk_display_get_n_monitors(display);
#endif
#ifdef GTK4
    GListModel *monitors = NULL;
    monitors = gdk_display_get_monitors(display);
    num_monitors = g_list_model_get_n_items(monitors);
#endif

    if (verbose)
        printf("Display %s - monitors: %d\n", gdk_display_get_name(display), num_monitors);
    if (num_monitors > 0) {
        for (gint i = 0; i < num_monitors; i++) {
            GdkMonitor *monitor;
            GdkRectangle geometry;
#ifdef GTK3
            monitor = gdk_display_get_monitor(display, i);
#endif
#ifdef GTK4
            monitor = (GdkMonitor*)g_list_model_get_item(monitors, i);
#endif

            gdk_monitor_get_geometry(monitor, &geometry);

            const char *name;
#ifdef GTK4
            name = gdk_monitor_get_connector(monitor);
#else
            name = "unknown connector";
#endif
            if (verbose) {
                printf("- monitor %d: [%s] [%s] [%s] - %dx%d-%d+%d - %s\n",
                        i, gdk_monitor_get_manufacturer(monitor), gdk_monitor_get_model(monitor),
                        name,
                        geometry.width, geometry.height, geometry.x, geometry.y,
#ifdef GTK3
                        gdk_monitor_is_primary(monitor)?"primary":"secondary"
#else
                        ""
#endif
                        );

            }
            else {
                standard_print(name, geometry.width, geometry.height, geometry.x, geometry.y);
            }
        }
    }

    if (open_new_display) {
        gdk_display_close(display);
    }
}

static const char * const modesetting_output_names[] = {
    [DRM_MODE_CONNECTOR_Unknown] = "None" ,
    [DRM_MODE_CONNECTOR_VGA] = "VGA" ,
    [DRM_MODE_CONNECTOR_DVII] = "DVI-I" ,
    [DRM_MODE_CONNECTOR_DVID] = "DVI-D" ,
    [DRM_MODE_CONNECTOR_DVIA] = "DVI-A" ,
    [DRM_MODE_CONNECTOR_Composite] = "Composite" ,
    [DRM_MODE_CONNECTOR_SVIDEO] = "SVIDEO" ,
    [DRM_MODE_CONNECTOR_LVDS] = "LVDS" ,
    [DRM_MODE_CONNECTOR_Component] = "Component" ,
    [DRM_MODE_CONNECTOR_9PinDIN] = "DIN" ,
    [DRM_MODE_CONNECTOR_DisplayPort] = "DP" ,
    [DRM_MODE_CONNECTOR_HDMIA] = "HDMI" ,
    [DRM_MODE_CONNECTOR_HDMIB] = "HDMI-B" ,
    [DRM_MODE_CONNECTOR_TV] = "TV" ,
    [DRM_MODE_CONNECTOR_eDP] = "eDP" ,
    [DRM_MODE_CONNECTOR_VIRTUAL] = "Virtual" ,
    [DRM_MODE_CONNECTOR_DSI] = "DSI" ,
    [DRM_MODE_CONNECTOR_DPI] = "DPI" ,
};

void drm_log_display_info(gboolean verbose)
{
    //char *path = "/dev/dri/card0";
    int drm_fd, i;
    for (i = 0; modules[i] != NULL ; i++) {
        //int drm_fd = open(path, O_RDWR);
        drm_fd = drmOpen(modules[i], NULL);
        if (drm_fd >= 0)
            break;

        if (verbose)
            printf("Unable to use module %s\n", modules[i]);
    }
    if (modules[i] == NULL) {
        printf("No module found - aborting\n");
        return;
    }
    if (verbose)
        printf("Using module %s\n", modules[i]);

    int ret ;

    ret = drmSetMaster(drm_fd);
    if (verbose)
        printf("drmSetMaster returned %d\n", ret);


    drmModeResPtr res = drmModeGetResources(drm_fd);
    if (res) {
        if (verbose)
            printf("Display fbs=%u crtcs=%u connectors=%u encoders=%u\n",
                res->count_fbs, res->count_crtcs, res->count_connectors, res->count_encoders);

//        for (int toto = 0 ; toto < res->count_crtcs ; toto++) {
//            drmModeCrtcPtr crtc = drmModeGetCrtc(drm_fd, res->crtcs[toto]);
//            if (crtc) {
//                printf("- CRTC#%u %ux%u+%u+%u\n",
//                        crtc->crtc_id,
//                        crtc->width, crtc->height,
//                        crtc->x, crtc->y);
//                drmModeFreeCrtc(crtc);
//            } else {
//                printf("- CRTC#%u [not found]\n", toto);
//            }
//        }

        for (int toto = 0 ; toto < res->count_connectors ; toto++) {
            drmModeConnectorPtr conn = drmModeGetConnector(drm_fd, res->connectors[toto]);
            if (!conn) {
                printf("- Connector#%u [not found]\n", toto);
                continue;
            }

            drmModeEncoderPtr enc = drmModeGetEncoder(drm_fd, conn->encoder_id);
            if (!enc) {
                if (verbose)
                    printf("- Connector#%u type=%u(%s) type ID %u [disconnected]\n",
                        conn->connector_id,
                        conn->connector_type,
                        modesetting_output_names[conn->connector_type],
                        conn->connector_type_id);
                drmModeFreeConnector(conn);
                continue;
            }

            uint32_t crtc_id = enc->crtc_id;
            drmModeFreeEncoder(enc);

            drmModeCrtcPtr crtc = drmModeGetCrtc(drm_fd, crtc_id);
            if (!crtc) {
                if (verbose)
                    printf("- Connector#%u type=%u(%s) type ID %u enc#%u crtc#%u [not found]\n",
                        conn->connector_id,
                        conn->connector_type,
                        modesetting_output_names[conn->connector_type],
                        conn->connector_type_id,
                        conn->encoder_id,
                        crtc_id);
                drmModeFreeConnector(conn);
                continue;
            }

            char name[20] ;
            snprintf(name, 20, "%s-%d", modesetting_output_names[conn->connector_type],
                    conn->connector_type_id);
            if (verbose) {
                printf("- Connector#%u type=%u(%s) type ID %u enc#%u crtc#%u %ux%u+%u+%u\n",
                    conn->connector_id,
                    conn->connector_type,
                    modesetting_output_names[conn->connector_type],
                    conn->connector_type_id,
                    conn->encoder_id,
                    crtc_id,
                    crtc->width, crtc->height,
                    crtc->x, crtc->y);
            }
            else {
                standard_print(name, crtc->width, crtc->height, crtc->x, crtc->y);
            }

            drmModeFreeCrtc(crtc);
            drmModeFreeConnector(conn);
        }
//
//        for (int toto = 0 ; toto < res->count_fbs ; toto++) {
//            drmModeFBPtr fb = drmModeGetFB(drm_fd, res->fbs[toto]);
//            if (fb) {
//                printf("- FB#%u %ux%u\n", fb->fb_id, fb->width, fb->height);
////                uint32_t pitch;
////                uint32_t bpp;
////                uint32_t depth;
////                /* driver specific handle */
////                uint32_t handle;
//
//            } else {
//                printf("- FB#%u [not found]\n", toto);
//            }
//        }
        drmModeFreeResources(res);
    }
    //close(drm_fd);
    if (ret == 0)
        drmDropMaster(drm_fd);
    drmClose(drm_fd);
}



static XRRCrtcInfo *crtc_from_id(Display *xdisplay, XRRScreenResources *xres, int id)
{
    int i;

    if (id == 0) {
        return NULL;
    }

    for (i = 0 ; i < xres->ncrtc ; ++i) {
        if (id == xres->crtcs[i]) {
            return XRRGetCrtcInfo(xdisplay, xres, xres->crtcs[i]);
        }
    }
    return NULL;
}

void x11_log_display_info(int verbose)
{
    Display *xdisplay;
    XRRScreenResources *xres;
    int i;
    Window root_window[MAX_SCREENS];

    xdisplay = XOpenDisplay(NULL);
    if (!xdisplay) {
        printf("could not connect to X-server\n");
        return;
    }
    int screen_count = ScreenCount(xdisplay);
    if (screen_count > MAX_SCREENS) {
        printf("Error too much screens: %d > %d\n", screen_count, MAX_SCREENS);
        XCloseDisplay(xdisplay);
        return;
    }

    for (i = 0; i < screen_count; i++)
        root_window[i] = RootWindow(xdisplay, i);

    XRRSelectInput(xdisplay, root_window[0], RRScreenChangeNotifyMask | RRCrtcChangeNotifyMask);

    xres = XRRGetScreenResourcesCurrent(xdisplay, root_window[0]);
    for (i = 0 ; i < xres->noutput; ++i) {
        XRROutputInfo *output = XRRGetOutputInfo(xdisplay, xres, xres->outputs[i]);
        XRRCrtcInfo *crtc = NULL;

        for (int j = 0; j < output->ncrtc; j++) {
            crtc = crtc_from_id(xdisplay, xres, output->crtcs[j]);
            if (crtc) {
                standard_print(output->name, crtc->width, crtc->height, crtc->x, crtc->y);
                XRRFreeCrtcInfo(crtc);
                break;
            }
        }
        XRRFreeOutputInfo(output);
    }
    XCloseDisplay(xdisplay);
}

static gboolean verbose = FALSE;
static gboolean loop = FALSE;
static gboolean x11, drm, dbus, gtk, open_new_display;

static gboolean timeout_cb(gpointer user_data)
{
    if (x11) {
        printf("X11 Monitors\n");
        x11_log_display_info(verbose);
        printf("/X11 Monitors\n") ;

        printf("\n");
    }

    if (drm) {
        printf("DRM Monitors\n");
        drm_log_display_info(verbose);
        printf("/DRM Monitors\n");

        printf("\n");
    }

    if (dbus) {
        printf("Mutter DBUS Monitors:\n");
        mutter_log_display_info(verbose);
        printf("/Mutter DBUS Monitors\n");

        printf("\n");
    }

    if (gtk) {
#ifdef GTK3
        printf("GTK3 Monitors:\n");
#endif
#ifdef GTK4
        printf("GTK4 Monitors:\n");
#endif
        gtk_log_display_info(verbose, open_new_display);
        printf("/GTK Monitors\n");
    }

    return G_SOURCE_CONTINUE;
}

int main(int argc, char ** argv)
{
    x11 = drm = dbus = gtk = open_new_display = FALSE;

    for (int i = 1 ; i < argc ; i++) {
        if (strcmp(argv[i], "-v") == 0)
            verbose = TRUE;
        if (strcmp(argv[i], "--loop") == 0)
            loop = TRUE;
        if (strcmp(argv[i], "--x11") == 0)
            x11 = TRUE;
        if (strcmp(argv[i], "--drm") == 0)
            drm = TRUE;
        if (strcmp(argv[i], "--dbus") == 0)
            dbus = TRUE;
        if (strcmp(argv[i], "--gtk") == 0)
            gtk = TRUE;
        if (strcmp(argv[i], "--reopen") == 0)
            open_new_display = TRUE;
    }

    if (!(x11 || drm || dbus || gtk)) {
        x11 = drm = dbus = gtk = TRUE;
    }

    gdk_set_allowed_backends("wayland,x11");

#ifdef GTK3
    gtk_init(NULL, NULL);
#endif

#ifdef GTK4
    gtk_init();
#endif

    if (!loop) {
        timeout_cb(NULL);
        return 0;
    }

    GMainLoop *gloop = g_main_loop_new(NULL, FALSE);
    g_timeout_add_seconds(2, timeout_cb, NULL);
    g_main_loop_run(gloop);
    g_main_loop_unref(gloop);
    return 0;
}
